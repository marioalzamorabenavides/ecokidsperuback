const config = require('../config/config');
const jwt = require('jsonwebtoken');
const SECRET_KEY = config.SECRET_KEY;

function verifytoken(req, res, next){

    const token = req.headers['authorization'];
    
    if(!token) return res.status(401).json({msg: "ingrese token"})
    
    try {
        
        const decoded = jwt.verify(token.split(' ')[1], SECRET_KEY);

        req.token_data = decoded
    } catch(err) {
        
        return res.status(401).json({msg: "Token inválido, cierre sesión sesión e ingrese nuevamente."})
    }
    
    next();
}

module.exports = verifytoken;