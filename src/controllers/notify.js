const notify = {}
const webpush = require('web-push');
const config = require('../config/config');
const nodemailer = require('nodemailer');
const { Pool } = require('pg');
const pool = new Pool({
    host: config.database.host,
    user: config.database.user,
    password: config.database.password,
    database: config.database.database,
    port: config.database.port
});

webpush.setVapidDetails(
    'mailto:ecokidsperu@gmail.com',
    config.publicKeyPush,
    config.privateKeyPush
);

notify.notifyMessage = async (param) => {

    const userTo = await pool.query(`
        select * from suscription where user_id = ${param.toUserId}
    `);

    const notificationPayload = {
        "notification": {
            "title": "¡Nuevo Mensaje!",
            "body": `Hola, ${param.fromName} ${param.fromLastname} quiere comunicarse contigo por el producto ${param.product}.`,
            "icon": "assets/icons/icon-512x512.png",
            // "data": [{ user: 'Mario', id: '2' }],
            "actions": [{
                action: 'showMessage',
                title: 'Ver Mensaje',
            }]
        }
    };

    userTo.rows.map(r => {
        webpush.sendNotification(
            JSON.parse(r.data), JSON.stringify(notificationPayload));
    });
}

const send_mail = async (to, asunto, body) => {

    const transporter = nodemailer.createTransport({
        host: config.servesmpt,
        secure: false,
        port: 587,
        auth: {
            user: config.llavesmptuser,
            pass: config.llavesmptpass
        }
    });

    const mailOptions = {
        from: 'ecokidsperu@gmail.com',
        to: to,
        subject: asunto,
        html: body
    };

    let obj;

    return new Promise(resolve => {
        transporter.sendMail(mailOptions, (error, info) => {

            if (error) {
                resolve({
                    status: 400,
                    msg: error
                });
            } else {
                resolve({
                    status: 200,
                    msg: info
                });
            }
        });
    });
}

module.exports = notify;