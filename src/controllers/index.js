const config = require('../config/config');
const { Pool } = require('pg');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const crypto = require('crypto-js');
const path = require('path');
const moment = require('moment');
const nodemailer = require('nodemailer');
const fs = require('fs')

const EXPIRE_IN = '3h';
const SECRET_KEY = config.SECRET_KEY;

const pool = new Pool({
    host: config.database.host,
    user: config.database.user,
    password: config.database.password,
    database: config.database.database,
    port: config.database.port
});

const me = async (req, res, next) => {

    const response = await pool.query(`
        select * from users where user_id=${req.token_data.user_id}
    `);

    delete response.rows[0]['password'];

    res.json(response.rows[0]);
}

const get_offers = async (req, res, next) => {

    const response = await pool.query(`
        select o.*, u.lastname, u.name, u.phone, u.type from offers o
        inner join users u on(u.user_id=o.user_id)
        where o.product_id=${req.params.id}
        order by register desc
    `);

    res.json(response.rows);
}

const get_orders_buyer = async (req, res, next) => {

    if (req.token_data.type == 'C' || req.token_data.type == 'A') {
        const response_ = await pool.query(`
            select ofs.offer_id as offer_id_compare, ofs.offer_price as offer_price, ofs.offer_comment as offer_comment, prod.*, us.lastname, us.name, us.distrito, us.email,us.colegio ,us.phone, us.type from products prod
            left join offers ofs on(prod.offer_id=ofs.offer_id or (ofs.user_id=${req.token_data.user_id} and ofs.product_id=prod.product_id and prod.offer_id is null))
            left join users us on(us.user_id=prod.user_id)
            where (prod.offer_id is null and prod.product_state='N') or ofs.user_id=${req.token_data.user_id}
            order by prod.product_register desc
        `);

        res.status(200).json(response_.rows);
    } else if (req.token_data.type == 'V' || req.token_data.type == 'A') {
        res.status(403).json({ msg: 'Metodo no perimitido por el usuario.' });
    } else {
        res.status(400).json({ msg: 'Ocurrió un error inesperado.' });
    }
}

const insert_offer = async (req, res, next) => {

    if (req.token_data.type == 'C' || req.token_data.type == 'A') {

        let dateTime = moment();

        const insert = [
            req.body.product_id,
            req.token_data.user_id,
            req.body.offer_price,
            req.body.offer_comment,
            dateTime,
            dateTime
        ];

        try {

            const response = await pool.query(
                `INSERT INTO 
                    offers(product_id,user_id,offer_price,offer_comment,offer_register,offer_update) 
                    VALUES($1,$2,$3,$4,$5,$6)
                RETURNING offer_id
                `, insert);

            res.status(200).json({
                product_id: insert[0],
                toUserId: req.body.user_id,
                toUserType: req.body.type,
                offer_comment: insert[3],
                offer_price: insert[2],
                offer_id_compare: response.rows[0]['offer_id']
            });

        } catch (e) {

            res.status(400).json({ msg: 'Ocurrió un error inesperado.' });

            console.log(e);
        }

    } else {
        res.status(403).json({ msg: 'Metodo no permitido por el usuario.' });
    }
}

const edit_offer = async (req, res, next) => {

    if (req.token_data.type == 'C' || req.token_data.type == 'A') {

        let dateTime = moment().format("YYYY-MM-DD HH:mm:ss");

        const data = {
            product_id: req.body.product_id,
            offer_id: req.body.offer_id,
            offer_price: req.body.offer_price,
            offer_comment: req.body.offer_comment,
            offer_update: dateTime
        };

        try {

            const response = await pool.query(
                `
                    UPDATE offers SET 
                        offer_price=${data.offer_price}, 
                        offer_comment='${data.offer_comment}',
                        offer_update='${data.offer_update}'
                    WHERE offer_id=${data.offer_id}
                `);

            res.status(200).json({
                product_id: data.product_id,
                toUserId: req.body.user_id,
                toUserType: req.body.type,
                offer_comment: data.offer_comment,
                offer_price: data.offer_price,
                offer_id_compare: data.offer_id
            });

        } catch (e) {

            res.status(400).json({ msg: 'Ocurrió un error inesperado.' });

            console.log(e);
        }

    } else {
        res.status(403).json({ msg: 'Metodo no permitido por el usuario.' });
    }
}

const getProducts = async (req, res, next) => {

    if (req.token_data.type == 'V' || req.token_data.type == 'A') {

        const response = await pool.query(`
        
            select a.*, count(b.product_id) as interesados from products a
            left join offers b on(a.product_id=b.product_id) 
            where a.user_id=${req.token_data.user_id}
            group by a.product_id
        `);

        const result = await search_users(response);

        res.status(200).json(result);

    } else if (req.token_data.type == 'V' || req.token_data.type == 'A') {

        res.status(403).json({ msg: 'Metodo no perimitido por el usuario.' });
    } else {

        res.status(400).json({ msg: 'Ocurrió un error inesperado.' });
    }
}

const search_users = async (param) => {

    if (param.rowCount > 0) {

        const data = await param.rows.map(async resp => {

            if (resp.product_state != 'X' && resp.product_state != 'N') {

                const response_ = await pool.query(`
                    select urs.name, urs.lastname, urs.user_id, urs.phone, urs.type, ofs.offer_price as offer_price from offers ofs
                    left join users urs on urs.user_id=ofs.user_id 
                    where ofs.offer_id=${resp.offer_id}
                `);

                resp.name = await response_.rows[0]['name'];
                resp.lastname = await response_.rows[0]['lastname'];
                resp.phone = await response_.rows[0]['phone'];
                resp.user_id_comp = await response_.rows[0]['user_id'];
                resp.type = await response_.rows[0]['type'];
                resp.offer_price = await response_.rows[0]['offer_price'];

                return resp;
            } else {
                return resp;
            }
        });

        return await Promise.all(data);

    } else {

        return [];
    }
}

const newProduct = async (req, res, next) => {

    if (req.token_data.type == 'V' || req.token_data.type == 'A') {

        let dateTime = moment();
        let arrImg = [];

        for (let i of req.files) {
            arrImg.push(i.filename);
        }

        const insert = [
            req.token_data.user_id,
            req.body.product_name,
            req.body.product_category,
            JSON.stringify(arrImg),
            req.body.product_unit,
            req.body.product_price_ref,
            req.body.product_price_vta,
            req.body.product_description,
            dateTime,
            dateTime,
            'N'
        ];

        try {

            const response = await pool.query(
                `INSERT INTO 
                    products(user_id,product_name,product_category,product_file,product_unit,product_price_ref,product_price_vta,product_description,product_register,product_update,product_state) 
                    VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11) 
                RETURNING product_id,user_id,offer_id,product_name,product_category,product_file,product_unit,product_price_ref,product_price_vta,product_description,product_register,product_update,product_state
                `, insert);

            response.rows[0]['interesados'] = 0;
            response.rows[0]['distrito'] = req.token_data.distrito;
            response.rows[0]['lastname'] = req.token_data.lastname;
            response.rows[0]['name'] = req.token_data.name;
            response.rows[0]['email'] = req.token_data.email;
            response.rows[0]['colegio'] = req.token_data.colegio;
            response.rows[0]['offer_comment'] = null;
            response.rows[0]['offer_id_compare'] = null;
            response.rows[0]['offer_price'] = null;
            response.rows[0]['phone'] = req.token_data.phone;
            response.rows[0]['product_recomendation'] = null;
            response.rows[0]['type'] = req.token_data.type;

            res.status(200).json({ msg: 'Se registró tu producto correctamente.', data: response.rows[0] });

        } catch (e) {

            res.status(400).json({ msg: 'Ocurrió un error inesperado.' });

            console.log(e);
        }

    } else {
        res.status(403).json({ msg: 'Metodo no permitido por el usuario.' });
    }
}

const getDistritos = async (req, res, next) => {

    const response = await pool.query(`
        select * from distritos
    `);

    res.json(response.rows);
}

const getColegios = async (req, res, next) => {

    const response = await pool.query(`
        select * from colegios
    `);

    res.json(response.rows);
}

const editProfiler = async (req, res, next) => {

    const dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
    const insert = [
        req.body.name,
        req.body.distrito,
        req.body.colegio,
        req.body.lastname,
        dateTime,
        '51' + req.body.phone,
        req.body.cod_alumno
    ];

    try {

        const response = await pool.query(`
            UPDATE users SET 
                name = '${insert[0]}',
                distrito = '${insert[1]}',
                colegio = '${insert[2]}',
                lastname = '${insert[3]}',
                update = '${insert[4]}',
                phone = '${insert[5]}',
                cod_alumno = '${insert[6]}'
            WHERE user_id=${req.token_data.user_id}
        `);

        res.status(200).json({ msg: 'Se realizaron tus cambios correctamente.' });

    } catch (error) {
        res.status(400).json({ msg: 'Ocurrió un error inesperado, inténtelo nuevamente.' });
    }

}

const signup = async (req, res, next) => {

    const salt = await bcrypt.genSalt(10);
    const password = await bcrypt.hash(req.body.password, salt);
    const dateTime = moment().format("YYYY-MM-DD HH:mm:ss");
    const insert = [
        req.body.name,
        req.body.distrito,
        req.body.colegio,
        req.body.lastname,
        req.body.email,
        // req.body.type, //usuario comprador o vendedor
        'A', //usuario all
        password,
        dateTime,
        dateTime,
        'P',
        '51' + req.body.phone,
        req.body.cod_alumno,
    ];
    const response = await pool.query('INSERT INTO users(name,distrito,colegio,lastname,email,type,password,register,update,state,phone,cod_alumno) VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12) RETURNING user_id', insert);

    if (response.rows[0].user_id) {

        const user_id = encodeURIComponent(Buffer.from(await encrypt(response.rows[0].user_id.toString())).toString('base64'));

        const bodyMail = `
            <h2>Hola, ${insert[0]} ${insert[3]}</h2>
            <p>
                Te has registrado en EcoKidsPerú, para activar tu cuenta has click 
                <a href="${config.url_backend}/api/activate_acount/${user_id}">Aquí</a>
            </p>            
            <p>
                Una vez que actives tu cuenta, serás redirigido al login el cual ingresarás tu cuenta 
                de correo y tu contraseña elegida.
            </p>
            <h4>Gracias.</h4>
        `;

        // console.log(bodyMail);

        // const bodyMail = `
        //     <h2>Hola, ${insert[0]} ${insert[3]}</h2>
        //     <p>Te has registrado como ${insert[5] == 'V' ? 'Vendedor' : 'Comprador'}, para activar tu cuenta has click <a href="${config.url_backend}:${config.puerto}/api/activate_acount/${user_id}">Aquí</a></p>            
        //     <p>Una vez que actives tu cuenta, serás redirigido al login el cual ingresarás tu cuenta de correo y tu contraseña elegida.</p>
        //     <h4>Gracias.</h4>
        // `;

        const send = await send_mail(insert[4], 'EcoKidsPerú::CONFIRMACION DE CUENTA', bodyMail);
        // const send={};
        // send.status = 200;

        if (send.status == 400) {

            res.status(400).json({ msg: 'Correo inválido, inténtelo nuevamente.' });
        } else {

            res.status(200).json({ msg: 'Se envió un correo de confirmación a ' + insert[4] + '.' });
        }
    } else {

        res.status(400).json({ msg: 'Ocurrió un error inesperado, inténtelo nuevamente.' });
    }
}

const activate_acount = async (req, res, next) => {

    try {
        const user_id = await decrypt(Buffer.from(decodeURIComponent(req.params.user_id), 'base64').toString('ascii'));

        const response = await pool.query(`UPDATE users SET state='A' WHERE user_id='${user_id}'`);

        // res.status(200).json({ msg: 'Se activó su cuenta correctamente.' });
        res.redirect(config.url_frontend + '/acceder')

    } catch (error) {

        res.status(400).json({ msg: 'Token de seguridad inválido.' });
    }
}

const signin = async (req, res, next) => {

    const values = [req.body.email, req.body.password];
    const response = await pool.query('SELECT * FROM users WHERE email = $1', [values[0]]);

    if (response.rows.length == 0) {

        res.status(401).json({ msg: "Usuario y/o contraseña son inválidas." });
    } else {

        const valid = await bcrypt.compare(values[1], response.rows[0].password);

        if (!valid) {
            res.status(401).json({ msg: "Usuario y/o contraseña son inválidas." });
        } else if (response.rows[0]['state'] == 'P') {
            res.status(401).json({ msg: "Debe confirmar su cuenta por correo." });
        } else {
            let obj_token = {
                user_id: response.rows[0]['user_id'],
                email: response.rows[0]['email'],
                name: response.rows[0]['name'],
                lastname: response.rows[0]['lastname'],
                type: response.rows[0]['type'],
                colegio: response.rows[0]['colegio'],
                phone: response.rows[0]['phone'],
                distrito: response.rows[0]['distrito']
            };

            const token = jwt.sign(obj_token, SECRET_KEY, { expiresIn: EXPIRE_IN });

            res.status(200).json({ token });
        }
    }
}

const verifyemail = async (req, res, next) => {

    const email = req.body.email;

    if (!email) res.status(200).json({ msg: 'email inválido.', status: 400 });

    const response = await pool.query('SELECT * FROM users WHERE email = $1', [email]);

    if (response.rows.length > 0) res.status(200).json({ msg: 'Ya existe el email.', status: 400 })

    else res.status(200).json({ msg: 'Email válido.' })
}

const acept_offers = async (req, res, next) => {

    if (req.token_data.type == 'V' || req.token_data.type == 'A') {

        try {

            const response = await pool.query(
                `   
                    UPDATE products set offer_id = ${req.body.offer_id}, product_state='A'
                    WHERE product_id=${req.body.product_id}
                `);

            res.status(200).json({
                offer_price: req.body.offer_price, //nuevo
                lastname: req.body.lastname, //nuevo
                name: req.body.name, //nuevo
                toUserId: req.body.user_id, //nuevo
                toUserType: req.body.type, //nuevo
                toUserphone: req.body.phone, //nuevo
                offer_id: req.body.offer_id,
                product_state: "A",
                product_id: req.body.product_id
            });

        } catch (e) {

            res.status(400).json({ msg: 'Ocurrió un error inesperado.' });

            console.log(e);
        }

    } else {
        res.status(403).json({ msg: 'Metodo no permitido por el usuario.' });
    }
}

const confirm_order = async (req, res, next) => {

    if (req.token_data.type == 'C' || req.token_data.type == 'A') {

        try {

            const response = await pool.query(
                `   
                    UPDATE products set product_state='R', product_recomendation = '${req.body.product_recomendation}'
                    WHERE product_id='${req.body.product_id}'
                `);

            res.status(200).json({
                product_id: req.body.product_id,
                product_state: 'R',
                toUserId: req.body.user_id,
                toUserType: req.body.type,
                toUserPhone: req.body.phone,
            });

        } catch (e) {

            res.status(400).json({ msg: 'Ocurrió un error inesperado.' });

            console.log(e);
        }

    } else {
        res.status(403).json({ msg: 'Metodo no permitido por el usuario.' });
    }
}

const cancel_order = async (req, res, next) => {

    if (req.token_data.type == 'V' || req.token_data.type == 'A') {

        try {

            const response = await pool.query(
                `   
                    UPDATE products set product_state='X'
                    WHERE product_id=${req.body.product_id}
                `);

            res.status(200).json({ msg: 'Se canceló su pedido correctamente.', data: { product_id: req.body.product_id, product_state: 'X' } });

        } catch (e) {

            res.status(400).json({ msg: 'Ocurrió un error inesperado.' });

            console.log(e);
        }

    } else {
        res.status(403).json({ msg: 'Metodo no permitido por el usuario.' });
    }
}

const getFile = async (req, res, next) => {

    if (fs.existsSync(path.join(__dirname, `../storage/img/${req.params.id}`))) {

        res.sendFile(path.join(__dirname, `../storage/img/${req.params.id}`));
    } else {

        res.sendFile(path.join(__dirname, `../storage/img/not-found.png`));
    }


}

const encrypt = async (value) => {

    return crypto.AES.encrypt(value, config.secretKeyForms).toString();
}

const decrypt = async (textToDecrypt) => {

    return crypto.AES.decrypt(textToDecrypt, config.secretKeyForms).toString(crypto.enc.Utf8);
}

const send_mail = async (to, asunto, body) => {

    const transporter = await nodemailer.createTransport({
        host: config.servesmpt,
        secure: false,
        port: 587,
        auth: {
            user: config.llavesmptuser,
            pass: config.llavesmptpass
        },
        tls: {
            rejectUnauthorized: false
        }
    });

    const mailOptions = {
        from: 'ecokidsperu@gmail.com',
        to: to,
        subject: asunto,
        html: body
    };

    let obj;

    return new Promise(resolve => {

        transporter.sendMail(mailOptions, (error, info) => {

            if (error) {

                resolve({
                    status: 400,
                    msg: error
                });
            } else {

                resolve({
                    status: 200,
                    msg: info
                })
            }
        })

    });
}

module.exports = {
    signup,
    verifyemail,
    signin,
    me,
    newProduct,
    getProducts,
    get_orders_buyer,
    insert_offer,
    edit_offer,
    get_offers,
    acept_offers,
    getFile,
    confirm_order,
    cancel_order,
    editProfiler,
    activate_acount,
    getDistritos,
    getColegios
}