const { Router } = require('express');
const router = Router();
const path = require('path');
const verifytoken = require('../controllers/verifytoken');

const controller = require('../controllers/index');

const multer = require('multer');
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(__dirname, '../storage/img'))
    },
    filename: function (req, file, cb) {
        cb(null, `${Date.now()}${file.originalname}`);
    }
})

const upload = multer({ storage: storage });

router.post('/signup', controller.signup);

router.post('/editProfiler', verifytoken, controller.editProfiler);

router.post('/verifyemail', controller.verifyemail);

router.post('/signin', controller.signin);

router.get('/getDistritos', controller.getDistritos);

router.get('/getColegios', controller.getColegios);

router.get('/activate_acount/:user_id', controller.activate_acount);

router.get('/me', verifytoken, controller.me);

router.post('/newProduct', verifytoken, upload.any([{ name: 'product_file', maxCount: 5 }]), controller.newProduct);

router.get('/getProducts', verifytoken, controller.getProducts);

router.get('/get_orders_buyer', verifytoken, controller.get_orders_buyer);

router.post('/sendOffer', verifytoken, controller.insert_offer);

router.post('/editOffer', verifytoken, controller.edit_offer);

router.get('/getOffers/:id', verifytoken, controller.get_offers);

router.post('/aceptOffers', verifytoken, controller.acept_offers);

router.get('/getFile/:id', controller.getFile);

router.post('/confirmReceived', verifytoken, controller.confirm_order);

router.post('/cancel_order', verifytoken, controller.cancel_order);

module.exports = router;