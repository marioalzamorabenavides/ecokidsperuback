const http = require('http');
const cors = require('cors');
const express = require('express');
const socketIO = require('socket.io');
const config = require('../src/config/config');
const routes = require('./routes/index');
// const notify = require('./controllers/notify');
// process.env.TZ = 'America/Lima';

const app = express();
const server = http.createServer(app);
const io = socketIO(server,{
  transports: ['polling'],
  serveClient: false,
  // below are engine.IO options
  pingInterval: 10000,
  pingTimeout: 5000,
  cookie: false

});
const sockets = {};
const sockets_ids = [];

//middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

//routes
app.use('/api', routes);

//cors socket
io.origins((origin, callback) => {

  // if (origin !== 'https://localhost:4200') {
  // 	return callback('origin not allowed', false);
  // }
  callback(null, true);
});

// io.emit('user_conect', sockets_ids);

//conection with socket
io.on('connection', (socket) => {

  socket.on('joinRoom', (data) => {
    
    socket.id = data.user_id;
    sockets["chat-user-" + socket.id] = socket;

    if (sockets_ids.indexOf(socket.id) === -1) sockets_ids.push(socket.id);
    
    io.emit('user_conect', sockets_ids);
  });

  socket.on('user_disconect', () => {

    delete sockets["chat-user-" + socket.id];

    if (sockets_ids.indexOf(socket.id) !== -1) sockets_ids.splice(sockets_ids.indexOf(socket.id), 1)

    io.emit('user_conect', sockets_ids);
  });

  socket.on('newProduct', (data) => {

    io.emit('newProduct', data);
  });

  socket.on('deleteProduct', (data) => {

    io.emit('deleteProduct', data);
  });

  socket.on('setOffer', (data) => {

    try {

      sockets[`chat-user-${data.toUserId}`].emit('setOffer', data);
    } catch (error) {

      console.log('usuario no conectado');
    }
  });

  socket.on('editOffer', (data) => {

    try {

      sockets[`chat-user-${data.toUserId}`].emit('editOffer', data);
    } catch (error) {

      console.log('usuario no conectado');
    }
  });

  socket.on('setAcepptOffer', (data) => {

    try {

      io.emit('setAcepptOffer', data);
    } catch (error) {

      console.log('usuario no conectado');
    }
  });

  socket.on('setConfirmPedidoComplete', (data) => {

    try {

      sockets["chat-user-" + data.toUserId].emit('setConfirmPedidoComplete', data);
    } catch (error) {

      console.log('usuario no conectado');
    }
  });

  socket.on('disconnect', () => {

    delete sockets["chat-user-" + socket.id];

    if (sockets_ids.indexOf(socket.id) !== -1) sockets_ids.splice(sockets_ids.indexOf(socket.id), 1)

    io.emit('disconnect', sockets_ids);
  });

});

server.listen(config.puerto, () => {

  console.log('started on port ' + config.puerto);
});
