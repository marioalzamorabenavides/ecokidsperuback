PGDMP     8                    x            ecokidsperu    12.4    12.4     '           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            (           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            )           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            *           1262    16721    ecokidsperu    DATABASE     �   CREATE DATABASE ecokidsperu WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Spain.1252' LC_CTYPE = 'Spanish_Spain.1252';
    DROP DATABASE ecokidsperu;
                postgres    false            +           0    0    ecokidsperu    DATABASE PROPERTIES     =   ALTER DATABASE ecokidsperu SET "TimeZone" TO 'America/Lima';
                     postgres    false            �            1259    16849    colegios    TABLE     �   CREATE TABLE public.colegios (
    escale character varying(100),
    colegio character varying(255),
    distrito character varying(255),
    detalle character varying(255)
);
    DROP TABLE public.colegios;
       public         heap    postgres    false            �            1259    16855 	   distritos    TABLE     G   CREATE TABLE public.distritos (
    distrito character varying(255)
);
    DROP TABLE public.distritos;
       public         heap    postgres    false            �            1259    16761    offers    TABLE       CREATE TABLE public.offers (
    offer_id integer NOT NULL,
    product_id integer,
    user_id integer,
    offer_price numeric(10,2),
    offer_register timestamp with time zone,
    offer_update timestamp with time zone,
    offer_comment character varying(255)
);
    DROP TABLE public.offers;
       public         heap    postgres    false            �            1259    16759    offers_offer_id_seq    SEQUENCE     �   CREATE SEQUENCE public.offers_offer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.offers_offer_id_seq;
       public          postgres    false    207            ,           0    0    offers_offer_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.offers_offer_id_seq OWNED BY public.offers.offer_id;
          public          postgres    false    206            �            1259    16750    products    TABLE       CREATE TABLE public.products (
    product_id integer NOT NULL,
    user_id integer,
    offer_id integer,
    product_name character varying(150),
    product_category character varying(150),
    product_file text,
    product_unit integer,
    product_price_ref numeric(10,2),
    product_price_vta numeric(10,2),
    product_description character varying(255),
    product_register timestamp with time zone,
    product_update timestamp with time zone,
    product_state "char",
    product_recomendation character varying(255)
);
    DROP TABLE public.products;
       public         heap    postgres    false            �            1259    16748    products_product_id_seq    SEQUENCE     �   CREATE SEQUENCE public.products_product_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.products_product_id_seq;
       public          postgres    false    205            -           0    0    products_product_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.products_product_id_seq OWNED BY public.products.product_id;
          public          postgres    false    204            �            1259    16724    users    TABLE     �  CREATE TABLE public.users (
    user_id integer NOT NULL,
    name character varying(50),
    distrito character varying(150),
    colegio character varying(200),
    lastname character varying(100),
    email character varying(100),
    type character(1),
    password character varying(255),
    register timestamp with time zone,
    update timestamp with time zone,
    state character(1),
    phone character varying(50),
    cod_alumno character varying(100)
);
    DROP TABLE public.users;
       public         heap    postgres    false            �            1259    16722    users_user_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.users_user_id_seq;
       public          postgres    false    203            .           0    0    users_user_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.users_user_id_seq OWNED BY public.users.user_id;
          public          postgres    false    202            �
           2604    16764    offers offer_id    DEFAULT     r   ALTER TABLE ONLY public.offers ALTER COLUMN offer_id SET DEFAULT nextval('public.offers_offer_id_seq'::regclass);
 >   ALTER TABLE public.offers ALTER COLUMN offer_id DROP DEFAULT;
       public          postgres    false    207    206    207            �
           2604    16753    products product_id    DEFAULT     z   ALTER TABLE ONLY public.products ALTER COLUMN product_id SET DEFAULT nextval('public.products_product_id_seq'::regclass);
 B   ALTER TABLE public.products ALTER COLUMN product_id DROP DEFAULT;
       public          postgres    false    204    205    205            �
           2604    16727    users user_id    DEFAULT     n   ALTER TABLE ONLY public.users ALTER COLUMN user_id SET DEFAULT nextval('public.users_user_id_seq'::regclass);
 <   ALTER TABLE public.users ALTER COLUMN user_id DROP DEFAULT;
       public          postgres    false    202    203    203            #          0    16849    colegios 
   TABLE DATA           F   COPY public.colegios (escale, colegio, distrito, detalle) FROM stdin;
    public          postgres    false    208   #       $          0    16855 	   distritos 
   TABLE DATA           -   COPY public.distritos (distrito) FROM stdin;
    public          postgres    false    209   }(       "          0    16761    offers 
   TABLE DATA           y   COPY public.offers (offer_id, product_id, user_id, offer_price, offer_register, offer_update, offer_comment) FROM stdin;
    public          postgres    false    207   	)                  0    16750    products 
   TABLE DATA              COPY public.products (product_id, user_id, offer_id, product_name, product_category, product_file, product_unit, product_price_ref, product_price_vta, product_description, product_register, product_update, product_state, product_recomendation) FROM stdin;
    public          postgres    false    205   &)                 0    16724    users 
   TABLE DATA           �   COPY public.users (user_id, name, distrito, colegio, lastname, email, type, password, register, update, state, phone, cod_alumno) FROM stdin;
    public          postgres    false    203   C)       /           0    0    offers_offer_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.offers_offer_id_seq', 1, false);
          public          postgres    false    206            0           0    0    products_product_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.products_product_id_seq', 1, false);
          public          postgres    false    204            1           0    0    users_user_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.users_user_id_seq', 1, false);
          public          postgres    false    202            �
           2606    16766    offers offers_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.offers
    ADD CONSTRAINT offers_pkey PRIMARY KEY (offer_id);
 <   ALTER TABLE ONLY public.offers DROP CONSTRAINT offers_pkey;
       public            postgres    false    207            �
           2606    16758    products products_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_pkey PRIMARY KEY (product_id);
 @   ALTER TABLE ONLY public.products DROP CONSTRAINT products_pkey;
       public            postgres    false    205            �
           2606    16732    users users_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (user_id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            postgres    false    203            #   \  x�}V=w�6��_����o�#%ۉ|$[�L�] 
�PC@
����1C��v�X��^P�H�L�� ��>�K}����|C�R�Fi2�tf�ԜL冟�~��J<q���M�5��Q˚����A+�<�"�ŀ��lH^z/knk���cI������Ò(faB򍰲���A_��=,��0e$�KW�7O�hli^�Y�8�ǚב}�6Zz�����]>��f�T���B�o�}�W�gY����9-�֢w�Џ�8"#�kN'���΅mp�7`v��K���T��~A�x,P�o���k:��Қc8�|���P♾��5-ʵ1���� ��6Z�U7� ː>�����JбQJ��E]N�J�Lu�-��->RU�â r \���*aA���T�XXщ��ռ�FsuHⴖ~"QH�3_y��0I��ó�捒Ns�<(/dY����j	�[�fr�u,w��Õ&z��Fq���U�rR�4IØ\6���`ɜ�k^B��caH��/��1��"T= 3�d9Z� ������Bӷ|��������9=n���w�zCG(5�:�|�C��ވ?��#��|�>/ �"]N������v:���F���I1���P��O��OlT����}���Sмe ��/)>�
d9�Z �?+B�Y��v	��]�׷��#?n�#�?
���'za6 ��+�����������hj쒎����>���Dxj.�~�q��Ν�M�>�^�y+�g�����%�7�|� �����R�-V;�W�L�Y���Y}�H��1�ț����d���)��̠]��M�$D!:��>�yה��hP'���ݍDWМ������*����ݺ4����l%t�^�yhE����KȾ�����+;��A�S�﹃o˺qY$�_�iց_�?��i2��9�?�e�H�+����4�d���I���
�n�D�4����HV]�C�SLG����E���F/̀ue!P��*��)��0�.u��jh��i��>���(�C���i7'�����u�:�+�ǎ�����������}n�VVe��h���@�l�	fN)[,��j)8<��|m��7��F��]���8;!/�%KXt��}:���E�n�S}L�].�T_\#��9�'Yآ�e	���9��P?/��X��n�g嗢}�ڪ�Χ^9T�F��^ ��^x�����V�ɃuM��EԽ�k��<i��`xoL���|^#�f3�)HD7�9}+�]���-�3�;��fqFޯ������s�Wb`�����S�U��i��F��Y9�,���Iۃ�m��>�x�ٗ��B-Jٟ�������T�Fg�����������      $   |   x��A
�0E��9�w���
Wn�q�#c'��z/&��=�s�p�F�����Y����d����	s�U-DY�q٬J�AwQ�z|�*7,ݿ}�a�)$S)�$NO5犙�F�>�q?�1      "      x������ � �             x������ � �            x������ � �     